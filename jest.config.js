module.exports = {
  "roots": [
    "./src"
  ],
  "transform": {
    "^.+\\.tsx?$": "ts-jest"
  },
  "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  "watchPathIgnorePatterns": [
    "testApp",
    "__mock"
  ],
  "collectCoverage": false,
  "collectCoverageFrom": [
    '**/*.{js,ts,jsx,tsx}',
    '!**/node_modules/**',
    '!**/dist/**',
  ],
  "moduleFileExtensions": [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
};
