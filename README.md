### Folder structure
```
core            - the core of project
config          - configuration of project 
src             - source
 | - actions    - app actions
 | - requests   - api requests
 | - reducers   - redux reducers
 | - components - react components
 | - pages      - react pages 
```
