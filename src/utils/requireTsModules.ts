import * as fs from 'fs';
import * as path from 'path';

import typescript from 'typescript';
import { promisify } from 'util';
import { createTmpFile } from './createTmpFile';

export async function requireTsModule(modulePath: string): Promise<any> {
  const readFile = promisify(fs.readFile);
  const writeFile = promisify(fs.writeFile);
  const tmp = await createTmpFile(modulePath);
  try {
    const content = await readFile(modulePath);
    const configFile = await readFile(path.join(__dirname, '../../tsconfig.json'));
    const cfg = typescript.parseConfigFileTextToJson('tsconfig.json', configFile.toString());;
    const rulesContentTranspiled = typescript
      .transpileModule(
        content.toString(),
        { compilerOptions: cfg.config.compilerOptions },
      );
    await writeFile(tmp.fd, rulesContentTranspiled.outputText);
    const module = require(tmp.path);
    tmp.cleanupCallback();
    return module;
  } catch (e) {
    tmp.cleanupCallback();
    throw e;
  }
}
