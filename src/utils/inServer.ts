export function inServer<T>(cb: () => T): void {
  if (typeof window === 'undefined') {
    cb();
  }
}
