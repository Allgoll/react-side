import { ENV } from '../resolver/env';

const consoleStamp = require('console-stamp');

consoleStamp(
  console,
  {
    pattern: 'HH:MM:ss',
    include: ['debug', 'info', 'warn', 'error', 'fatal', 'log'],
    level: ENV.logLevel ? ENV.logLevel : 'log',
  },
);

export function log(...args: any[]) {
  console.log(...args);
}
