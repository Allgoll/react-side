import * as path from 'path';
import tmp from 'tmp';

interface TmpData {
  path: string;
  fd: number;
  cleanupCallback: () => void;
}

export function createTmpFile(filePath: string) {
  return new Promise<TmpData>((resolve, reject) => {
    tmp.file({ dir: path.dirname(filePath) }, (err, path, fd, cleanupCallback) => {
      if (err) {
        reject(err);
      }
      resolve({ path, fd, cleanupCallback });
    });
  });
}
