import { inServer } from '../inServer';

describe('utils inServer', () => {
  test('dont call callback in browser', () => {
    const mock = jest.fn();
    inServer(mock);
    expect(mock).not.toBeCalled();
  });
});
