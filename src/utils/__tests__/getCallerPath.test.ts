import { getCallerPath } from '../getCallerPath';

describe('utils getCallerPath', () => {
  test('return current file path', () => {
    function testCaller() {
      return getCallerPath();
    }
    expect(testCaller()).toBe(__filename);
  });
});
