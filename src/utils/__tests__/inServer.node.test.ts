/**
 * @jest-environment node
 */
import { inServer } from '../inServer';

describe('utils inServer', () => {
  test('call callback in node', () => {
    const mock = jest.fn();
    inServer(mock);
    expect(mock).toBeCalled();
  });
});
