/**
 * @jest-environment node
 */
import { inClient } from '../inClient';

describe('utils inCLient', () => {
  test('dont call callback on node', () => {
    const mock = jest.fn();
    inClient(mock);
    expect(mock).not.toBeCalled();
  });
});
