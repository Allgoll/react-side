import { spawn } from 'child_process';
import fs, { existsSync } from 'fs';
import path from 'path';
import { copyFile } from '../copyFile';

const SOURCE_FILE = path.join(__dirname, 'source');
const DESTINATION_FILE = path.join(__dirname, 'destination');

const clean = () => {
  if (existsSync(DESTINATION_FILE)) {
    spawn('rm', [DESTINATION_FILE]);
  }
  if (existsSync(SOURCE_FILE)) {
    spawn('rm', [SOURCE_FILE]);
  }
};

describe('utils copyFile', () => {
  afterEach(clean);
  beforeEach(clean);
  test('copy file to another directory', (end) => {
    clean();
    fs.writeFileSync(SOURCE_FILE, 'test_data');
    copyFile(SOURCE_FILE, DESTINATION_FILE, (err) => {
      expect(err).toBeUndefined();
      expect(existsSync(DESTINATION_FILE)).toBeTruthy();
      expect(existsSync(SOURCE_FILE)).toBeTruthy();
      end();
    });
  });

  test('copy file to another directory without callback', () => {
    clean();
    fs.writeFileSync(SOURCE_FILE, 'test_data');
    copyFile(SOURCE_FILE, DESTINATION_FILE);
    expect(existsSync(DESTINATION_FILE)).toBeTruthy();
    expect(existsSync(SOURCE_FILE)).toBeTruthy();
  });

  test('return err if no file exists', (end) => {
    clean();
    copyFile(SOURCE_FILE, DESTINATION_FILE, (err?: Error) => {
      expect(err).not.toBeUndefined();
      expect(err!.message)
        .toBe(
        'ENOENT: no such file or directory, open ' +
          `'${SOURCE_FILE}'`,
        );
      end();
    });
  });

  test('return err if destination not acceptable', (end) => {
    clean();
    fs.writeFileSync(SOURCE_FILE, 'test_data');
    copyFile(SOURCE_FILE, '', (err?: Error) => {
      expect(err).not.toBeUndefined();
      expect(err!.message)
        .toBe(
          'ENOENT: no such file or directory, open ' +
          `''`,
        );
      end();
    });
  });
});
