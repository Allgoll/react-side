import { requireTsModule } from '../requireTsModules';
import path from 'path';

describe('utils requireTsModules', () => {
  test('require module from ts-module', (end) => {
    const modulePath = path.join(__dirname, '../inClient.ts');
    requireTsModule(modulePath).then((module) => {
      expect(module.inClient.name).toBe('inClient');
      end();
    });
  });

  test('catch when module not found', (end) => {
    const promise = requireTsModule('some.ts');
    promise.catch((error) => {
      expect(error.message).toEqual(`ENOENT: no such file or directory, open 'some.ts'`);
      end();
    });
  });
});
