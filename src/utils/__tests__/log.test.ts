describe('utils log', () => {
  test('log smth with timestamp', () => {
    const log = require('../log').log;
    console.log = jest.fn();
    log('test stamp');
    expect(console.log).toBeCalledWith('test stamp');
  });
});
