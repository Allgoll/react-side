import { inClient } from '../inClient';

describe('utils inCLient', () => {
  test('call callback in browser', () => {
    const mock = jest.fn();
    inClient(mock);
    expect(mock).toBeCalled();
  });
});
