import { createTmpFile } from '../createTmpFile';
import path from 'path';
import fs from 'fs';

const TMP_FILE_PATH = path.join(__dirname, 'tmp');

describe('utils createTmpFile', () => {
  test('create tmp file', (end) => {
    createTmpFile(TMP_FILE_PATH).then((data) => {
      expect(data.path).toMatch(TMP_FILE_PATH);
      expect(data.fd).toBeDefined();
      expect(data.cleanupCallback).toBeDefined();
      end();
      data.cleanupCallback();
    });
  });

  test('return valid file descriptor', (end) => {
    createTmpFile(TMP_FILE_PATH).then((data) => {
      fs.read(data.fd, Buffer.from([]), 0, 0, 0, (err, bytes) => {
        expect(err).toBeNull();
        expect(bytes).toBe(0);
        end();
        data.cleanupCallback();
      });
    });
  });

  test('remove file when cleanup is called', (end) => {
    createTmpFile(TMP_FILE_PATH).then((data) => {
      expect(fs.existsSync(data.path)).toBeTruthy();
      data.cleanupCallback();
      expect(fs.existsSync(data.path)).toBeFalsy();
      end();
    });
  });

  test('throw error when wrong path provided', (end) => {
    createTmpFile('/test/test/test').catch((err) => {
      expect(err.message)
        .toMatch(/^ENOENT: no such file or directory/);
      end();
    });
  });
});
