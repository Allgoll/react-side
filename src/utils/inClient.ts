export function inClient<T>(cb: () => T): void {
  if (typeof window !== 'undefined') {
    cb();
  }
}
