import * as fs from 'fs';

export function copyFile(source: string, target: string, cb?: (err?: Error) => void) {
  let cbCalled = false;

  const rd = fs.createReadStream(source);
  rd.on('error', err => done(err));

  const wr = fs.createWriteStream(target);
  wr.on('error', err => done(err));
  wr.on('close', () => done());

  rd.pipe(wr);

  function done(err?: Error) {
    if (cb && !cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}
