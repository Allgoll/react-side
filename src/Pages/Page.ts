import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactDOMServer from 'react-dom/server';

import { hot } from 'react-hot-loader';
import { inServer } from '../utils/inServer';
import { inClient } from '../utils/inClient';
import { defaultGenerateStaticMarkup } from '../server/defaultMarkup';
import { getCallerPath } from '../utils/getCallerPath';

export interface IPageConfiguration {
  module: NodeJS.Module;
  path: string;
  node: () => HTMLElement | null;
}

export interface IInjectedPage {
  _content: string;
  _path: string;
  _filePath: string;
  name?: string;
}

export function page(config: Partial<IPageConfiguration>) {
  const callerPath = getCallerPath(); // it's a kind of dark magic, don't MOVE it, don't touch it
  if (!callerPath) { throw Error('magic isn\'t working here'); }
  const pageFileName = /.*\/(.*\.js)$/.exec(callerPath)![1];
  return (reactClass: any) => {

    inServer(() => {
      reactClass._content = defaultGenerateStaticMarkup(
        ReactDOMServer.renderToString(React.createElement(reactClass)),
        '',
        pageFileName,
      );
      reactClass._path = config.path || '/';
      reactClass._filePath = callerPath;
    });

    inClient(() => {
      ReactDOM.hydrate(
        React.createElement(reactClass),
        config.node!(),
      );
    });

    return hot(config.module)(reactClass);
  };
}
