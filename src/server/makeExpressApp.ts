import express from 'express';

export function makeExpressApp() {
  const app = express();
  app.use('/public', express.static('dist'));
  app.use((req, res, next) => {
    res.setHeader('content-type', 'text/html');
    next();
  });
  return app;
}
