export function defaultGenerateStaticMarkup(content: string, context: string, name: string) {
  return `
<!doctype html>
<head>
    <script async type="text/javascript" src="/public/bundle/${name}"></script>
    <title>ReactSideApp</title>
    ${context}
</head>
<body>
    <div id="root">${content}</div>
</body>
`;
}
