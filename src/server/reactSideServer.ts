import { Express } from 'express';
import { WebpackProvider } from '../compilers/webpack/webpackProvider';
import { IInjectedPage } from '../Pages';
import { ENV } from '../resolver/env';
import { log } from '../utils/log';
import { makeExpressApp } from './makeExpressApp';
import { SideServer } from './SideServer';

require('css-modules-require-hook')({
  generateScopedName: '[name]__[local]___[hash:base64:5]',
});

export function reactSideServer(pages?: any[]) {
  return new SideServer(pages);
}
