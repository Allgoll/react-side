import { agent } from 'supertest';
import { makeExpressApp } from '../makeExpressApp';

describe('server makeExpressApp', () => {
  test('return express application instance', () => {
    const app = makeExpressApp();
    expect(app).toBeDefined();
  });

  test('application answered with correct content-type', (end) => {
    const app = makeExpressApp();
    app.get('/test', (req, res) => res.send('ok'));

    agent(app)
      .get('/test')
      .expect('Content-Type', /text\/html/)
      .expect('Content-Length', '2')
      .expect(200, end);
  });
});
