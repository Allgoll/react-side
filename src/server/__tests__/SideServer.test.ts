jest.mock('../../resolver/env', () => ({
  ENV: { hot: true },
}));
jest.mock('../../utils/log', () => ({
  log: () => ({}),
}));

import path from 'path';
import { agent } from 'supertest';
import { IInjectedPage } from '../../Pages';
import { serverBridge } from '../bridge';
import { SideServer } from '../SideServer';

const MOCK_PAGES: IInjectedPage[] = [
  { _filePath: 'fileOne', _content: 'testOne', _path: '/pageOne' },
  { _filePath: 'fileTwo', _content: 'testTwo', _path: '/pageTwo' },
];

describe('server SideServer', () => {
  test('pages initialization', () => {
    const server: any = new SideServer(MOCK_PAGES);
    expect(server.app).toBeDefined();
    expect(server.coldRun).toBeFalsy();
    expect(server.pagesPaths)
      .toStrictEqual(MOCK_PAGES.map(({ _filePath }) => _filePath));
    expect(server.bridge).toBe(serverBridge);
    expect(server.running).toBeFalsy();
    expect(server.userWebpackConfigurationPath).toBeUndefined();
  });

  test('MOCK_PAGES validation', () => {
    expect(MOCK_PAGES
      .map(SideServer.isValidPage)
      .every(t => t),
    ).toBeTruthy();
    expect(SideServer.isValidPage({})).toBeFalsy();
  });

  test('attach pages', (end) => {
    const server: any = new SideServer(MOCK_PAGES);
    const app = server.prepare();
    agent(app)
      .get('/pageOne')
      .expect(200, 'testOne', end);
  });

  test('return 404 on non-existent page request', (end) => {
    const server: any = new SideServer(MOCK_PAGES);
    const app = server.prepare();
    agent(app)
      .get('/pageThree')
      .expect(404, end);
  });

  test('attach webpack configuration', (end) => {
    const server: any = new SideServer(MOCK_PAGES);
    const app = server.attachWebpackConfiguration(
      path.join(__dirname, '__mockWebpackConfiguration.js'),
    );
    expect
  });
});
