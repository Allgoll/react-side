import { EventEmitter } from 'events';
import { serverBridge } from '../bridge';

const processMock: any = new EventEmitter();
processMock.send = (data: any) => processMock.emit('message', data);

describe('server bridge', () => {
  test('attach process to the bridge', () => {
    serverBridge.attachProcess(process as any);
    expect(serverBridge.process).toBe(process);
  });

  test('emit pages', () => {
    serverBridge.attachProcess(processMock);
    serverBridge.emitPagePathsAndConfig(['page1'], 'cfgPath');
    serverBridge.on('pages', (data) => {
      expect(data).toBe([['page1'], 'cfgPath']);
    });
  });

  test('do not emit not a server message', () => {
    serverBridge.attachProcess(processMock);
    const mock = jest.fn();
    processMock.send({ asd: 'asd' });
    serverBridge.on('pages', mock);
    processMock.emit('message', { asd: 'asd' });
    expect(mock).not.toBeCalled();
  });

  test('emit server message directly from process', () => {
    serverBridge.attachProcess(processMock);
    const mockData = [['page1'], 'cfgPath'];
    processMock.send({ type: 'pages', data: mockData });
    serverBridge.on('pages', (data) => {
      expect(data).toBe(mockData);
    });
  });
});
