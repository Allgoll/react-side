import { ChildProcess } from 'child_process';
import { EventEmitter } from 'events';

export enum EServerMessageTypes {
  pages = 'pages',
  config = 'config',
}

export interface IServerMessage {
  type: EServerMessageTypes;
  data: any;
}

class ServerBridgeSingleton extends EventEmitter {
  process: ChildProcess | undefined;

  private isServerMessage(message: any): message is IServerMessage {
    if (typeof message === 'object' && typeof message.type === 'string') {
      return Object.keys(EServerMessageTypes).includes(message.type);
    }
    return false;
  }

  private emitServerMessage(message: IServerMessage) {
    this.emit('pages', message.data);
  }

  public emitPagePathsAndConfig(pagePaths: string[], cfg?: string) {
    if (process.send) {
      process.send({
        type: EServerMessageTypes.pages,
        data: [pagePaths, cfg],
      });
    }
  }

  public attachProcess(process: ChildProcess) {
    this.process = process;
    this.process.on('message', (message: any) => {
      this.isServerMessage(message) && this.emitServerMessage(message);
    });
  }
}

export const serverBridge = new ServerBridgeSingleton();
