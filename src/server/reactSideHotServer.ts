import * as http from 'http';
import { hotModules } from '../compilers/webpack/hotModules';
import { WebpackProvider } from '../compilers/webpack/webpackProvider';
import { ENV } from '../resolver/env';
import { log } from '../utils/log';
import { makeExpressApp } from './makeExpressApp';

export function reactSideHotServer(pages?: any[], userConfig?: any) {
  const app = makeExpressApp();
  const server = http.createServer(app);

  if (pages) {
    const webpackProvider = new WebpackProvider(userConfig, pages);

    const { devMiddleware, hotMiddleware } = hotModules(webpackProvider.getConfig());
    app.use(devMiddleware);
    app.use(hotMiddleware);
  }

  log(`starting webpack hot server at ${ENV.host} ${ENV.port + 1}`);
  server.listen(ENV.port + 1, ENV.host);
}
