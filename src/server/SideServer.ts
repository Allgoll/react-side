import { Express } from 'express';
import * as path from 'path';
import { WebpackProvider } from '../compilers/webpack/webpackProvider';
import { IInjectedPage } from '../Pages';
import { ENV } from '../resolver/env';
import resolver from '../resolver/resolver';
import { log } from '../utils/log';
import { serverBridge } from './bridge';
import { makeExpressApp } from './makeExpressApp';

export class SideServer {
  private readonly app: Express = makeExpressApp();
  private webpackProvider: WebpackProvider;
  private readonly coldRun: boolean;
  private readonly pagesPaths: string[];
  private readonly bridge = serverBridge;
  private running = false;
  private userWebpackConfigurationPath?: string;

  private static isValidPage(page: IInjectedPage) {
    if (!page._path || !page._content) {
      log(
        `Component: ${page.name || page.constructor.name} isn't a` +
        `valid page component, perhaps you` +
        `mistakenly forgot to use a @page decorator above a page component in ${page._filePath}`,
      );
      return false;
    }
    return true;
  }

  public constructor(private readonly pages: IInjectedPage[] = []) {
    this.pagesPaths = pages.map(page => page._filePath);
    this.webpackProvider = new WebpackProvider(undefined, this.pagesPaths);
    this.coldRun = process.argv[process.argv.length - 1] === 'pre';
  }

  public prepare(): Express {
    if (this.pages.length === 0) {
      log('You forgot to attach any page,' +
        'please attach a page in your main.ts and restart the server');
    }
    if (this.coldRun) {
      this.bridge.emitPagePathsAndConfig(this.pagesPaths, this.userWebpackConfigurationPath);
      return this.app;
    }
    ENV.hot ? this.runHot() : this.runStatic();
    this.running = true;
    log(`=================== Ready to code ===================`);
    log('');
    return this.app;
  }

  public attachWebpackConfiguration(configurationPath: string): this {
    let resolvedPath = undefined;
    if (path.isAbsolute(configurationPath)) {
      resolvedPath = configurationPath;
    } else {
      resolvedPath = path.resolve(path.join(resolver.dist, 'server/core', configurationPath));
    }
    this.webpackProvider = new WebpackProvider(resolvedPath, this.pagesPaths);
    this.userWebpackConfigurationPath = resolvedPath;
    return this;
  }

  private runStatic() {
    this.webpackProvider.run().then(() => this.attachPages());
  }

  private runHot() {
    this.attachPages();
  }

  private attachPages() {
    this.pages.forEach((page) => {
      if (SideServer.isValidPage(page)) {
        this.app.get(page._path, (req, res) => res.send(page._content));
        log(`attached page ${page._path}`);
      }
    });
  }
}
