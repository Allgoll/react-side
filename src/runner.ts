import { ChildProcess, fork } from 'child_process';
import * as path from 'path';
import { TypescriptProvider } from './compilers/typescript/typescriptProvider';
import { hotModulesEvents } from './compilers/webpack/hotModules';
import { ENV } from './resolver/env';
import resolver from './resolver/resolver';
import { serverBridge } from './server/bridge';
import { reactSideHotServer } from './server/reactSideHotServer';
import { log } from './utils/log';

(function run() {
  const tsProvider = new TypescriptProvider();
  log('starting...');
  let childProcess: ChildProcess | undefined;

  if (ENV.hot) {
    tsProvider.onChange(() => {
      if (!childProcess) {
        log('starting up application server');
        childProcess = fork(`${path.join(resolver.dist, 'server/core/main.js')}`, ['pre']);
        serverBridge.attachProcess(childProcess);
        serverBridge.once('pages', ([pages, config]) =>
          reactSideHotServer(pages, config),
        );
      }
    });
    tsProvider.watch();
    hotModulesEvents.addListener('change', () => {
      log('application recompiled, restarting...');
      childProcess!.kill();
      childProcess = fork(`${path.join(resolver.dist, 'server/core/main.js')}`);
      serverBridge.attachProcess(childProcess);
    });
  } else {
    log('compiling typescript');
    tsProvider.run();
    log('attaching pages, and start up server');
    fork(`${path.join(resolver.dist, 'server/core/main.js')}`);
  }
})();
