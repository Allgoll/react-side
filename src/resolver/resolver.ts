import * as path from 'path';

export default {
  root: path.resolve('./'),
  source: path.resolve('./src'),
  dist: path.resolve('./dist'),
  core: path.resolve('./core'),
  config: path.resolve('./config'),
  webpackRules: path.join(path.resolve('./config', 'webpack.rules.ts')),
  nodeModules: path.join(path.resolve('./'), 'node_modules'),
};
