export const ENV = {
  hot: Boolean(process.env.HOT),
  port: getPort(),
  host: getHost(),
  logLevel: process.env.LOG_LEVEL,
};

function getPort(): number {
  const port = Number(process.env.PORT);
  if (port && (isNaN(port) || port === 0)) {
    console.log(`could't parse PORT value: ${port}, application will start at 3000`);
    return 3000;
  }

  return port || 3000;
}

function getHost(): string {
  const host = process.env.HOST;
  if (host && host.length === 0) {
    console.log(`could't parse HOST value: ${host}, application will start at localhost`);
    return 'localhost';
  }

  return host || 'localhost';
}
