import * as ts from 'typescript';
import { createWatchHost } from './createWatchHost';

export function createWatchProgram(options: ts.CompilerOptions) {
  ts.createWatchProgram(createWatchHost(options));
}
