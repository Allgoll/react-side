import * as ts from 'typescript';
import { log } from '../../../utils/log';

export function getPatchedTsSystem() {
  return {
    ...ts.sys,
    clearScreen: () => {
    },
    write: log,
    newLine: '',
  };
}
