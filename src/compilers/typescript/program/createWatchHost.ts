import * as ts from 'typescript';
import { moveTransformer } from '../moveTransformer';
import { createWatchProgramWithTransformers } from './createWatchProgramWithTransformers';
import { diagnosticReporterFactory } from './diagnosticReporterFactory';
import { getPatchedTsSystem } from './getPatchedTsSystem';
import { getRootFiles } from './getRootFiles';
import { watchStatusReporterFactory } from './watchStatusReporterFactory';

/**
 * Watch host with custom reporter, scope of files, and transformers
 */
export function createWatchHost(options: ts.CompilerOptions) {
  return ts.createWatchCompilerHost(
    getRootFiles(),
    options,
    getPatchedTsSystem(),
    createWatchProgramWithTransformers([moveTransformer]),
    diagnosticReporterFactory(options),
    watchStatusReporterFactory(),
  );
}
