import * as ts from 'typescript';

/**
 * Create a typescript BuilderProgram with injected transformers
 */
export function createWatchProgramWithTransformers(
  beforeTransformers: ts.TransformerFactory<ts.SourceFile>[] = [],
  afterTransformers: ts.TransformerFactory<ts.SourceFile>[] = [],
): ts.CreateProgram<ts.BuilderProgram> {
  return (rootNames, options, host, oldProgram, configFileParsingDiagnostics) => {
    const oldProgramRef = oldProgram as ts.EmitAndSemanticDiagnosticsBuilderProgram;
    const prg = ts.createEmitAndSemanticDiagnosticsBuilderProgram(
      rootNames!, options!, host, oldProgramRef, configFileParsingDiagnostics,
    );
    const oldEmit = prg.emit;
    const newEmit: ts.Program['emit'] = (
      targetSourceFile,
      writeFile,
      cancellationToken,
      emitOnlyDtsFiles,
      customTransformers = {},
    ) => {
      return oldEmit(
        targetSourceFile,
        writeFile,
        cancellationToken,
        emitOnlyDtsFiles,
        {
          ...customTransformers,
          before: beforeTransformers.concat(customTransformers.before || []),
          after: afterTransformers.concat(customTransformers.after || []),
        },
      );
    };
    prg.emit = newEmit;
    return prg;
  };
}
