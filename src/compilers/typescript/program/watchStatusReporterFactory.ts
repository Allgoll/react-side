import * as ts from 'typescript';
import { log } from '../../../utils/log';
import { tsEventEmitter } from './tsEventEmitter';

/**
 * Custom watchStatusReporter with tsEventEmitter.
 * There is only one way to report about compilation changes inside a status reporter
 */
export function watchStatusReporterFactory() {
  return (diagnostic: ts.Diagnostic, newLine: string, options: ts.CompilerOptions) => {
    log(diagnostic.messageText);
    // fire only if complete successfully
    if (diagnostic.code === 6194 || diagnostic.code === 6193) {
      tsEventEmitter.emit('change');
    }
  };
}
