import * as ts from 'typescript';
import { log } from '../../../utils/log';

export function diagnosticReporterFactory(options: ts.CompilerOptions) {
  return (diagnostic: ts.Diagnostic) => {
    log('==== Typescript Error ====\n\n');
    log(ts.formatDiagnosticsWithColorAndContext(
      [diagnostic],
      ts.createCompilerHost(options),
    ));
    log('==========================');
  };
}
