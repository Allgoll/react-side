import * as ts from 'typescript';
import resolver from '../../../resolver/resolver';

export function getRootFiles() {
  return ts
    .sys
    .readDirectory(resolver.root, ['ts', 'tsx'], [resolver.nodeModules], []);
}
