import * as fs from 'fs';
import * as path from 'path';
import * as ts from 'typescript';
import resolver from '../../resolver/resolver';
import { log } from '../../utils/log';
import { tsEventEmitter } from './program/tsEventEmitter';
import { createWatchProgram } from './program/typescriptProgram';

export class TypescriptProvider {

  private readonly options: ts.CreateProgramOptions;

  public constructor() {
    const { config } = ts.readConfigFile(
      path.join(resolver.root, 'tsconfig.json'),
      (path: string) => fs.readFileSync(path).toString(),
    );
    const { options } =
      ts.convertCompilerOptionsFromJson(config.compilerOptions, '', 'tsconfig.json');
    this.options = {
      options,
      rootNames: ['*'],
    };
  }

  public run() {
    const host = ts.createCompilerHost(this.options.options);
    const files = host.readDirectory!(resolver.root, ['ts', 'tsx'], [resolver.nodeModules], []);
    const prg = ts.createProgram(files, this.options.options, host);
    prg.emit();
  }

  public watch() {
    log('starting typescript service');
    createWatchProgram(this.options.options);
  }

  public onChange(cb: () => void) {
    tsEventEmitter.addListener('change', cb);
  }
}
