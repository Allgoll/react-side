import * as path from 'path';
import * as ts from 'typescript';
import resolver from '../../resolver/resolver';
import { copyFile } from '../../utils/copyFile';
import { walk } from './walk';

export function moveTransformer(context: ts.TransformationContext) {
  const outDir = context.getCompilerOptions().outDir;
  return (sourceFile: ts.SourceFile) => {
    if (outDir) {
      walk(sourceFile, (node) => {
        switch (node.kind) {
          case ts.SyntaxKind.ImportDeclaration:
            const importRef: ts.ImportDeclaration = node as ts.ImportDeclaration;
            const moduleSpecifierText = importRef.moduleSpecifier.getText();
            if (moduleSpecifierText) {
              processReference(moduleSpecifierText, sourceFile, outDir);
            }
            break;
          case ts.SyntaxKind.ExternalModuleReference:
            const ref: ts.ExternalModuleReference = node as ts.ExternalModuleReference;
            const externalModuleText = ref.expression!.getText();
            if (externalModuleText) {
              processReference(ref.expression!.getText(), sourceFile, outDir);
            }
            break;
        }
      });
    }

    return sourceFile;
  };
}

function processReference(ref: string, sourceFile: ts.SourceFile, outDir: string) {
  // when traverse ast we receive smth like that:
  // import * as styles from './some.css';
  // here moduleSpecifier(ref) will be a string literal './some.css'
  // so we need sanitize it and remove quotes ' " ` transform string literal... to string literal
  const externalFile = ref.replace(/['"`]/ig, '');
  const ext = path.extname(externalFile);
  if (ext && !/\.(tsx?|jsx?|config)$/.test(ext)) {
    resolveAndCopy(sourceFile, externalFile, outDir);
  }
}

function resolveAndCopy(sourceFile: any, externalFile: string, outDir: string) {
  const parseSourceFilePath = path.parse(sourceFile.path);
  const externalFilePath = path.join(parseSourceFilePath.dir, externalFile);
  const lowerRootPath = resolver.root.toLowerCase();
  const externalPathRelativeRoot = path.relative(
    lowerRootPath,
    externalFilePath,
  );
  const dest = path.join(
    lowerRootPath,
    outDir,
    externalPathRelativeRoot,
  );
  copyFile(externalFilePath, dest);
}
