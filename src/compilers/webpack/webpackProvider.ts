import * as path from 'path';
import webpackConfig from './webpack.config';
import webpack from 'webpack';
import { createReporter } from './createReporter';

export class WebpackProvider {
  private readonly webpackUserConfig = {
    webpackWatchReporter: createReporter as any,
  };
  private readonly webpackConfig: webpack.Configuration = {};

  constructor(webpackUserConfigPath: string | undefined, private pages: string[]) {
    if (webpackUserConfigPath) {
      if (path.extname(webpackUserConfigPath) === '.ts') {
        this.webpackUserConfig = require(webpackUserConfigPath.replace(/ts$/, 'js')).default;
      } else {
        this.webpackUserConfig = require(webpackUserConfigPath + '.js').default;
      }
    }
    this.webpackConfig = webpackConfig(this.webpackUserConfig, pages);
  }

  public run() {
    return new Promise(resolve =>
      webpack(this.webpackConfig).run((err, data) => {
        if (this.webpackUserConfig.webpackWatchReporter) {
          this.webpackUserConfig.webpackWatchReporter()(err, data);
        } else {
          createReporter()(err, data);
        }
        if (data.hasErrors()) { throw Error('webpack errors'); }
        resolve();
      }),
    );
  }

  public getConfig() {
    return this.webpackConfig;
  }
}
