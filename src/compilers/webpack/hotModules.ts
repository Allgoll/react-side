import webpack from 'webpack';
import { EventEmitter } from 'events';
import { log } from '../../utils/log';

const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

export const hotModulesEvents = new EventEmitter();

export function hotModules(config: webpack.Configuration) {
  const compiler = webpack(config);
  const devMiddleware = webpackDevMiddleware(compiler, {
    reporter: createReporter(),
    publicPath: config.output!.publicPath,
    logLevel: 'error',
    logTime: true,
    writeToDisk: true,
    stats: 'errors-only',
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  });

  const hotMiddleware = webpackHotMiddleware(compiler, {
    log: console.log,
    path: '/__webpack_hmr',
    heartbeat: 10 * 1000,
    noInfo: false,
  });

  return {
    devMiddleware,
    hotMiddleware,
  };
}

function createReporter() {
  return (middlewareOptions: any, options: any) => {
    const { state, stats } = options;

    if (state) {
      const displayStats = (middlewareOptions.stats !== false);

      if (displayStats) {
        if (stats.hasErrors()) {
          log('====== Webpack Error =====\n', stats.toString(middlewareOptions.stats), '\n');
          log('==========================');
        } else if (stats.hasWarnings()) {
          log(stats.toString(middlewareOptions.stats));
        } else {
          // log(stats.toString(middlewareOptions.stats));
        }
      }

      let message = 'Compiled successfully.';

      if (stats.hasErrors()) {
        message = 'Failed to compile.';
      } else if (stats.hasWarnings()) {
        message = 'Compiled with warnings.';
      }
      log(message);
      hotModulesEvents.emit('change');
    } else {
      log('Compiling...');
    }
  };
}
