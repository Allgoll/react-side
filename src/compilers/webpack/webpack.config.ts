import * as path from 'path';

import webpack, { RuleSetRule } from 'webpack';
import resolver from '../../resolver/resolver';

export interface IExternal {
  entry: string[];
  env: {
    IS_BROWSER: boolean,
  };
}

const hmrstr =
  'webpack-hot-middleware/client?' +
  'path=http://localhost:3001/__webpack_hmr&' +
  'timeout=2000&reload=true&' +
  'overlay=true&' +
  'overlayWarnings=true';

const config = (external: IExternal, userRules: RuleSetRule[]): webpack.Configuration => ({
  entry: external.entry.reduce(
    (acc, val) =>
      Object.assign(acc, { [path.basename(val)]: [val, hmrstr] }),
    {},
  ),
  mode: 'development',
  resolve: {
    extensions: ['.js'],
    modules: [
      resolver.nodeModules,
      path.join(__dirname, '../../../../node_modules'),
    ],
  },
  resolveLoader: {
    extensions: ['.js'],
    modules: [
      resolver.nodeModules,
      path.join(__dirname, '../../../../node_modules'),
    ],
  },
  devtool: 'cheap-source-map',
  output: {
    filename: 'bundle/[name]',
    path: resolver.dist,
    chunkFilename: '[name].bundle.js',
    publicPath: 'http://localhost:3001/',
    hotUpdateChunkFilename: 'hot/[hash].hot-update.js',
    hotUpdateMainFilename: 'hot/[hash].hot-update.json',
  },
  module: {
    rules: [
      ...userRules,
    ],
  },
  optimization: {
    splitChunks: {
      name: 'vendor',
    },
  },
  node: {
    http: 'empty',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.IS_BROWSER': true,
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.IgnorePlugin(/\.test\.(tsx?|jsx?)$/),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
});

export default function (userWebpackConfig: any | undefined, externals: string[]) {
  const module = userWebpackConfig && userWebpackConfig.module;
  return config(
    {
      env: (process.env as any),
      entry: externals,
    },
    module ? module.rules : [],
  );
}
