import { Compiler } from 'webpack';
import util from 'util';

export function createReporter(): Compiler.Handler {
  let prevHash = '';
  return (err, { hash, endTime, startTime, compilation }: any) => {
    if (err) { console.log(err.message); }
    if (hash !== prevHash) {
      console.log(`static bundle compiled in ${endTime - startTime}ms`);
      if (compilation.errors) { printErrors(compilation.errors); }
      prevHash = hash;
    }
  };
}

function printErrors(errs: any[]) {
  if (errs && Array.isArray(errs)) {
    errs.forEach((err) => {
      if (err.message) {
        util.print(err.message);
      }
    });
  }
}
