module.exports = (name) => `
{
  "name": "${name}",
  "version": "0.0.1",
  "description": "",
  "scripts": {
    "test": "jest --watch",
    "start": "react-side run",
    "dev": "react-side run -h"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "react": "^16.4.1",
    "react-dom": "^16.4.1",
    "react-side": "git+ssh://git@gitlab.com:Allgoll/react-side.git#build",
    "typescript": "^2.9.2"
  },
  "devDependencies": {
    "@types/react": "^16.3.17",
    "@types/react-dom": "^16.0.7",
    "@types/webpack": "^4.4.0",
    "@types/jest": "^23.1.0",
    "jest": "^23.1.0",
    "ts-jest": "^22.4.6",
    "tslint": "^5.10.0",
    "tslint-config-airbnb": "^5.9.2"
  }
}
`;