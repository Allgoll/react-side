module.exports = () => `
import { Configuration } from 'react-side';
/**
 * This is default webpack configuration you only could editing rules for now
 * it's already configured for css modules
 * */

export default {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[name]__[local]___[hash:base64:5]'
            }
          }
        ]
      }
    ]
  }
} as Configuration;
`;
