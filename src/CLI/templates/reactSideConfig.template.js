module.exports = () => `
/*
 * this file is used conventionally to setting up and managing server
 * you may use all of your pages here.
*/
import { reactSideServer } from 'react-side/dist/src/server';
import { NewPage } from "../src/pages/NewPage";

reactSideServer([
  NewPage
]);
`;