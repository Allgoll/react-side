module.exports = () => `
import * as React from "react";
import { page } from 'react-side';

@page({
  module,
  path: '/',
  node: () => document.getElementById('root'),
})
export class NewPage extends React.PureComponent {
  render() {
    return (
      <div>Hello from react-side</div>
    );
  }
}
`;