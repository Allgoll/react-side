import { spawn } from 'child_process';
import fs from 'fs';
import path from 'path';
import chalk from 'chalk';

function spawnWithArgs(args: string[]) {
  return spawn(
    'node',
    ['rs.js', ...args],
    {
      cwd: path.join(__dirname, '../'),
    },
  );
}

const helpInfo = `To create a project use 'new' command and specify a project directory
${chalk.blue('  react-side new <project directory>')}

For example
${chalk.blue('  react-side new my-app')}

For more information use --help
`;

describe('CLI tools', () => {
  test('output version', (end) => {
    const mock = jest.fn();
    const process = spawnWithArgs(['--version']);
    process.stdout.on('data', (out) => {
      expect(out.toString().trim()).toMatch(/\d+\.\d+\.\d+/);
      end();
    });
    process.stderr.on('data', mock);
    expect(mock).not.toBeCalled();
  });

  test('help information', (end) => {
    const mock = jest.fn();
    const process = spawnWithArgs([]);
    let output = '';
    process.stdout.on('data', (out) => {
      output += out.toString();
    });
    process.stderr.on('data', mock);
    process.on('close', () => {
      expect(output).toBe(helpInfo);
      expect(mock).not.toBeCalled();
      end();
    });
  });

  test('application creation', (end) => {
    const testAppPath = __dirname + '/../testApp';
    const existsInApp = (testPath: string) =>
      fs.existsSync(path.join(testAppPath, testPath));
    const mock = jest.fn();
    const clean = () =>
      fs.existsSync(testAppPath)
        ? spawn('rm', ['-rf', testAppPath])
        : undefined;

    clean();
    const process = spawnWithArgs(['new', 'testApp', '--skip-install']);
    process.on('close', () => {
      expect(fs.existsSync(testAppPath)).toBeTruthy();
      expect(existsInApp('config/webpack.config.ts')).toBeTruthy();
      expect(existsInApp('core/main.ts')).toBeTruthy();
      expect(existsInApp('src/actions')).toBeTruthy();
      expect(existsInApp('src/components')).toBeTruthy();
      expect(existsInApp('src/pages/MainPage.tsx')).toBeTruthy();
      expect(existsInApp('src/reducers')).toBeTruthy();
      expect(existsInApp('src/requests')).toBeTruthy();
      expect(existsInApp('tslint.json')).toBeTruthy();
      expect(existsInApp('package.json')).toBeTruthy();
      expect(existsInApp('tsconfig.json')).toBeTruthy();
      expect(mock).not.toBeCalled();
      end();
      clean();
    });
    process.stderr.on('data', mock);
  });
});
