#!/usr/bin/env node

/* script deps */
const childProcess = require('child_process');
const consoleStamp = require('console-stamp');
const program      = require('commander');
const chalk        = require('chalk');
const path         = require('path');
const fs           = require('fs');

/* templates */
const tsconfigJsonTemplate     = require('./templates/tsconfigJson.template');
const packageJsonTemplate      = require('./templates/packageJson.template');
const reactSideConfigTemplate  = require('./templates/reactSideConfig.template');
const pageTemplate             = require('./templates/page.template');
const webpackTemplate          = require('./templates/webpack.template');
const tslintTemplate           = require('./templates/tslint.template');

/* constants */
const workDir = path.resolve('./');

/* commander setup */
program
  .version('0.0.1')
  .option('-l --log <n>', 'log level: log info warn')
  .usage('project_name [options]');

program
  .command('new <project>')
  .option('--skip-install', 'Skip dependency installation')
  .description('create a new project')
  .action(newApp);

program
  .command('run')
  .option('-h, --hot', 'Launch application with hot reloading')
  .action(run);

program
  .on('command:*', noAction);

program.parse(process.argv);

if (process.argv.length <= 2) { noAction(); }

/* actions */
function beforeStart() {
  consoleStamp(
    console,
    {
      pattern: 'HH:MM:ss',
      include: ['debug', 'info', 'warn', 'error', 'fatal', 'log'],
      level: program.log ? program.log : 'log',
    }
  )
}

/* NO ACTION */
function noAction() {
  console.log('To create a project use \'new\' command and specify a project directory');
  info('  react-side new <project directory>');
  console.log();
  console.log('For example');
  info('  react-side new my-app');
  console.log();
  console.log('For more information use --help');
}
/* RUN ACTION */
function run(cmd) {
  beforeStart();
  childProcess.exec('npm root', (error, stdout, stderr) => {
    if (error || stderr) {
      console.error(error || stderr);
      return;
    }
    const projectRoot = path.join(path.resolve(stdout.trim()), '../');
    const runner = path.join(projectRoot, 'node_modules/react-side/dist/src/runner.js');
    if (projectRoot && fs.existsSync(runner)) {
      if (cmd.hot) {
        childProcess.fork(runner, [], {env: {...process.env, HOT: 'true'}});
      } else {
        childProcess.fork(runner);
      }
    } else {
      console.error("couldn't locate react-side application");
    }
  });
}

/* NEW ACTION */
function newApp(projectName, cmd) {
  beforeStart();
  console.log(program.log);
  info(`projectName: ${projectName}`);
  info(`workDir: ${workDir}`);
  const projectDirectory = makeProjectDir(projectName);
  makePackageJson(projectName, projectDirectory);
  makeTsConfig(projectDirectory);
  info(`scaffolding application`);
  makeSrcDirectory(projectDirectory);
  fs.mkdirSync(path.join(projectDirectory, 'config'));
  fs.writeFileSync(path.join(projectDirectory, 'config', 'webpack.config.ts'), webpackTemplate());
  fs.writeFileSync(path.join(projectDirectory, 'tslint.json'), tslintTemplate());
  makeCoreDirectory(projectDirectory);
  info(`installing packages`);
  if (!cmd.skipInstall) {
    childProcess.execSync('npm i', {cwd: projectDirectory, stdio: 'inherit'});
  }
  info('done')
}

function makeTsConfig(projectDirectory) {
  const tsconfigJsonContent = tsconfigJsonTemplate();
  fs.writeFileSync(path.join(projectDirectory, 'tsconfig.json'), tsconfigJsonContent);
  info(`created tsconfig.json`);
}

function makePackageJson(projectName, projectDirectory) {
  const packageJsonContent = packageJsonTemplate(projectName);
  fs.writeFileSync(path.join(projectDirectory, 'package.json'), packageJsonContent);
  info(`created package.json`);
}

function makeProjectDir(projectName) {
  const projectDirectory = path.join(workDir, projectName);
  if (fs.existsSync(projectDirectory)) {
    console.error(chalk.red(`project directory ${projectName} already exist in path`));
    process.exit(1);
  }
  fs.mkdirSync(projectDirectory);
  info(`successfully created project directory ${projectDirectory}`);
  return projectDirectory;
}

function makeSrcDirectory(projectDirectory) {
  const srcPath = path.join(projectDirectory, 'src');
  fs.mkdirSync(srcPath);
  fs.mkdirSync(path.join(srcPath, 'actions'));
  fs.mkdirSync(path.join(srcPath, 'requests'));
  fs.mkdirSync(path.join(srcPath, 'reducers'));
  fs.mkdirSync(path.join(srcPath, 'components'));
  fs.mkdirSync(path.join(srcPath, 'pages'));
  fs.writeFileSync(path.join(srcPath, 'pages', 'MainPage.tsx'), pageTemplate());
}

function makeCoreDirectory(projectDirectory) {
  const corePath = path.join(projectDirectory, 'core');
  fs.mkdirSync(corePath);
  fs.writeFileSync(path.join(corePath, 'main.ts'), reactSideConfigTemplate());
}

function info(descr) {
  console.info(chalk.blue(descr));
}
